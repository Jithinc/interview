<?php

namespace Larashop\Http\Controllers\Admin;

use Larashop\Models\Order;
use Larashop\Models\Services_list;
use Larashop\Models\OrderDetail;
use Illuminate\Http\Request;
use Larashop\Http\Controllers\Controller;
use Auth;
use Illuminate\Support\Facades\DB;

class OrdersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
        public function index()
        {
            $id=Auth::user()->id;
            $orders = Order::where('customer_id','=',$id)->get();

            $params = [
                'title' => 'Orders Listing',
                'orders' => $orders,
            ];

            return view('admin.orders.orders_list')->with($params);
        }
        public function addorder(Request $request,$id)
        {
            $service = Services_list::where('ser_id','=',$id)->first();
            $cid=Auth::user()->id;
            $order=Order::create([
                'service_id' => $service->ser_id,
                'service_name' => $service->service_name,
                'customer_id' => $cid,
                'total_amount' => $service->price,
                ]);
            
            return redirect()->route('orders.index')->with('success', "The order  has successfully been added.");
            
        }
        public function view(Request $request,$id)
        {
            $order=Order::findOrFail($id);
            $image=DB::table('order_details')->where([
                ['service_id', '=', $id],
                ['user_id', '=', Auth::user()->id],
                ['type','=','image']
            ])->get();
            $video=DB::table('order_details')->where([
                ['service_id', '=', $id],
                ['user_id', '=', Auth::user()->id],
                ['type','=','video']
            ])->get();
            $params = [
                'title' => 'Order Details',
                'image' =>$image,
                'video'=>$video,
                'order' => $order,
                ];
            return view('admin.orders.orders_details')->with($params);
            
        }
        public function viewuserorder(Request $request,$id)
        {
            $order=Order::findOrFail($id);
            $image=DB::table('order_details')->where([
                ['service_id', '=', $id],
            ])->get();
            $params = [
                'title' => 'Order Details',
                'image' =>$image,
                'order' => $order,
                ];
            return view('admin.orders.user_orderdetails')->with($params);
            
        }
        public function vieworders(Request $request)
        {
            $order = DB::table('orders')
            ->join('users', 'users.id', '=', 'orders.customer_id')
            ->select('orders.*', 'users.name')
            ->get();
            $params = [
                'title' => 'User Orders',
                'order' => $order,
                ];
            return view('admin.orders.user_orders')->with($params);
            
        }
        public function destroy(Request $request,$id)
        {
            $order=Order::findOrFail($id);
            $order->delete();
			$order=OrderDetail::findOrFail($id);
			
			return  redirect()->route('orders.index')->with('success', "The  <strong>Order</strong> has successfully been archived.");
			
            
        }
        public function addfile(Request $request,$id)
        {
            $this->validate($request, [
                        
                'file' => 'required|mimes:png,jpg,jpeg,svg|max:1024',
            ]);
            $t='image';
            $imageName=time().'.'.$request->file->getClientOriginalExtension();
            $request->file('file')->move(public_path('images'), $imageName);
            $order = OrderDetail::create([
                'file'=>$imageName,
                'service_id'=>$id,
                'user_id'=>Auth::user()->id,
                'type'=>$t
            ]);
    
            return redirect()->route('orders.index')->with('success', "The Image  Upload Successfully Completed.");
        }
        public function addvideofile(Request $request,$id)
        {
            $this->validate($request, [
                        
                'video' => 'required|mimes:mp4,mkv,flv,avi|max:10240'
            ]);    
            $t='video';
            $fileName=time().'.'.$request->video->getClientOriginalExtension();
            $request->file('video')->move(public_path('videos'), $fileName);
            $order = OrderDetail::create([
                'file'=>$fileName,
                'service_id'=>$id,
                'user_id'=>Auth::user()->id,
                'type'=>$t,
            ]);
    
            return redirect()->route('orders.index')->with('success', "The Video  Upload Successfully Completed.");
        }
    }
