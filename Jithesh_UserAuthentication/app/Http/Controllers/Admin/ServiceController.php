<?php

namespace Larashop\Http\Controllers\Admin;

use Larashop\Models\Services_list;
use Larashop\Models\Service;
use Illuminate\Http\Request;

use Larashop\Http\Controllers\Controller;

use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Support\Facades\DB;
class ServiceController extends Controller
{
    public function __construct()
	{
		
		$this->middleware('permission:create', ['only' => ['create-service', 'store-service']]);
		
		$this->middleware('permission:edit', ['only' => ['edit-service', 'update-service']]);
		
		$this->middleware('permission:delete', ['only' => ['show-service', 'delete']]);
		
	}
	
	
	
	/**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
	
	public function index()
	{
		
		$services = Service::all();
		
		
		$params = [
		'title' => 'Service Listing',
		'services' => $services,
		];
		
		
		return view('admin.services.services_list')->with($params);
		
	}
	
	
	
	/**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
	
	public function create()
	{
		
		$params = [
		'title' => 'Create Service',
		];
		
		
		return view('admin.services.services_create')->with($params);
		
	}
	
	
	
	/**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
	
	public function store(Request $request)
	{
		
		$this->validate($request, [
		'name' => 'required|unique:services',
		]);
		
		
		$service = Service::create([
		'name' => $request->input('name'),
		]);
		
		
		return redirect()->route('services.index')->with('success', "The service <strong>$service->name</strong> has successfully been created.");
		
	}
	public function newservice($id){
		$service=Service::findOrFail($id);
		$params = [
			'title' => 'Add service',
			'service' => $service,
			];
		return view('admin.services.services_view_create')->with($params);
	}
	public function createservice(Request $request,$id)
	{
		
		$this->validate($request, [
		'service_name' => 'required|unique:services_lists',
		'price' => 'required',
		'description' => 'required',
		]);
		
		
		$service = Services_list::create([
		'service_name' => $request->input('service_name'),
		'price' => $request->input('price'),
		'description' => $request->input('description'),
		'id'=>$id
		]);
		
		
		return redirect()->route('services.index')->with('success', "The service  has successfully been created.");
		
	}
	
	/**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
	
	public function show($id)
	{
		
		try{
			
			$service = Service::findOrFail($id);
			
			
			$params = [
			'title' => 'Delete service',
			'service' => $service,
			];
			
			
			return view('admin.services.services_delete')->with($params);
			
		}
		
		catch (ModelNotFoundException $ex) 
		{
			
			if ($ex instanceof ModelNotFoundException)
			{
				
				return response()->view('errors.'.'404');
				
			}
			
		}
		
	}
	public function editform($ser_id)
	{
		
		try{
			
			$service = DB::table('services_lists')
			->where('ser_id','=',$ser_id)->get();
			
			$params = [
				'title' => 'Edit service',
				'service' => $service,
				];
				
				
				return view('admin.services.services_view_edit')->with($params);
			
		}
		
		catch (ModelNotFoundException $ex) 
		{
			
			if ($ex instanceof ModelNotFoundException)
			{
				
				return response()->view('errors.'.'404');
				
			}
			
		}
		
	}
	public function view($id)
	{
		
		try{
			$services = DB::table('services_lists')
				  ->where('id','=',$id)->get();
		    $service = DB::table('services')
				  ->where('id','=',$id)->first();
			$name1=$service->name;
			$params = [
				'title' => $name1,
				'service' => $service,
				'services' => $services,
				];
			return view('admin.services.services_view')->with($params);
				}
		
		catch (ModelNotFoundException $ex) 
		{
			
			if ($ex instanceof ModelNotFoundException)
			{
				
				return response()->view('errors.'.'404');
				
			}
			
		}
		
	}
	
	/**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
	
	public function edit($id)
	{
		
		try
		{
			
			$service = Service::findOrFail($id);
			
			
			$params = [
			'title' => 'Edit service',
			'service' => $service,
			];
			
			
			return view('admin.services.services_edit')->with($params);
			
		}
		
		catch (ModelNotFoundException $ex) 
		{
			
			if ($ex instanceof ModelNotFoundException)
			{
				
				return response()->view('errors.'.'404');
				
			}
			
		}
		
	}
	public function editservice($id,$ser_id)
	{
		
		try
		{
			
			$service = DB::table('services_lists')
			->where('ser_id','=',$ser_id)->get();			
			$params = [
			'services' => 'Edit Service',
			'service' => $service,
			];
			
			
			return view('admin.services.services_view_edit')->with($params);
			
		}
		
		catch (ModelNotFoundException $ex) 
		{
			
			if ($ex instanceof ModelNotFoundException)
			{
				
				return response()->view('errors.'.'404');
				
			}
			
		}
		
	}
	
	
	/**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
	
	public function update(Request $request, $id)
	{
		
		try
		{
			
			$this->validate($request, [
			'name' => 'required|unique:services,name,'.$id,
			]);
			
			
			$service = Service::findOrFail($id);
			
			
			$service->name = $request->input('name');
						
			
			$service->save();
			
			
			return redirect()->route('services.index')->with('success', "The service <strong>$service->name</strong> has successfully been updated.");
			
		}
		
		catch (ModelNotFoundException $ex) 
		{
			
			if ($ex instanceof ModelNotFoundException)
			{
				
				return response()->view('errors.'.'404');
				
			}
			
		}
		
	}
	public function updateservice(Request $request,$id,$ser_id)
	{
		
		try
		{
			
			$this->validate($request, [
			'service_name' => 'required',
			'price' => 'required',
			'description' => 'required']);	
			
			$service_name = $request->input('service_name');
			$price = $request->input('price');
			$description= $request->input('description');
			
			DB::table('services_lists')->where('ser_id', $ser_id)->update(['service_name' => $service_name,'price'=>$price,
			'description'=>$description]);
			//$service->save();
			
			
			return redirect()->route('services.index')->with('success', "The service  has successfully been updated.");
			
		}
		
		catch (ModelNotFoundException $ex) 
		{
			
			if ($ex instanceof ModelNotFoundException)
			{
				
				return response()->view('errors.'.'404');
				
			}
			
		}
		
	}
	
	public function destroyservice($id,$ser_id)
	{
		
		try
		{
			$service=Services_list::where('ser_id',$ser_id)->forcedelete();
			return redirect()->route('services.index')->with('success', "The  <strong>Service</strong> has successfully been archived.");
		}
		
		catch (ModelNotFoundException $ex) 
		{
			
			if ($ex instanceof ModelNotFoundException)
			{
				
				return response()->view('errors.'.'404');
				
			}
			
		}
		
	}
	/**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
	
	public function destroy($id)
	{
		
		try
		{
			
			$service = Service::findOrFail($id);
			
			
			$service->delete();
			
			
			return redirect()->route('services.index')->with('success', "The  <strong>Service</strong> has successfully been archived.");
			
		}
		
		catch (ModelNotFoundException $ex) 
		{
			
			if ($ex instanceof ModelNotFoundException)
			{
				
				return response()->view('errors.'.'404');
				
			}
			
		}
		
	}}
