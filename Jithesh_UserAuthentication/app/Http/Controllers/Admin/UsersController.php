<?php

namespace Larashop\Http\Controllers\Admin;

use Larashop\Models\User;
use Larashop\Models\Role;
use Larashop\Models\Permission;
use Larashop\Models\Conversation;
use Larashop\Models\Order;
use Illuminate\Http\Request;
use Larashop\Http\Controllers\Controller;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Auth;
use Illuminate\Support\Facades\Input;
use Response;
use Illuminate\Support\Facades\DB;
class UsersController extends Controller
{
    /**
     * Instantiate a new UserController instance.
     */
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users=DB::table('users')->get();
        $params = [
            'title' => 'Users Listing',
            'users' => $users,
        ];
        return view('admin.users.users_list')->with($params);
    }

    public function view()
    {
        $userId = Auth::id();
        $user=User::findOrFail($userId);
        $doc=DB::table('customers')
        ->where('user_id','=',$userId)
        ->get();
        $params = [
            'title' => 'User Profile',
            'docs'=>$doc,
            'user' => $user,
        ];
        return view('admin.users.users_profile')->with($params);
    }
    public function viewinbox()
    {
        $id=Auth::user()->id;
        //$message= DB::table('message')->where('recipient_id','=',$rec_id)->get();
        //$sen_id=$message->sender_id;
        //$sender= DB::table('users')->where('id','=',$sen_id)->get();
        $conv = DB::table('conversations')
        ->select('conv_id')
        ->where('id1','=',$id)
        ->first();
        $message = DB::table('message')
        ->where('conv_id','=',$conv->conv_id)
        ->orderBy('created_at', 'desc')
        ->get();
        $admin=User::find(1);
        $params = [
            'messages'=>$message,
            'admin'=>$admin,
            'title' => 'Messages',
        ];
        return view('admin.users.user_inbox')->with($params);
    }
    public function viewmessage()
    {
        $id=Auth::user()->id;
        //$message= DB::table('message')->where('recipient_id','=',$rec_id)->get();
        //$sen_id=$message->sender_id;
        //$sender= DB::table('users')->where('id','=',$sen_id)->get();
        $conv = DB::table('conversations')
        ->join('users', 'users.id', '=', 'conversations.id1')
        ->select('conversations.*', 'users.name', 'users.id','users.image')
        ->get();
        $params = [
            'messages'=>$conv,
            'title' => 'Messages',
        ];
        return view('admin.users.admin_inbox')->with($params);
    }
    public function viewmes(Request $request,$conv_id)
    {
        //$message= DB::table('message')->where('recipient_id','=',$rec_id)->get();
        //$sen_id=$message->sender_id;
        //$sender= DB::table('users')->where('id','=',$sen_id)->get();
        $conv = DB::table('conversations')
        ->where('conv_id','=',$conv_id)
        ->first();
        $user=User::where('id','=',$conv->id1)->first();
        $message = DB::table('message')
        ->where('conv_id','=',$conv_id)
        ->orderBy('created_at','desc')
        ->get();
        $params = [
            'messages'=>$message,
            'user'=>$user,
            'title' => 'Messages',
        ];
        return view('admin.users.admin_view')->with($params);
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $permission = Permission::all();

        $params = [
            'title' => 'Create User',
            'permission' => $permission,
        ];

        return view('admin.users.users_create')->with($params);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $admin=Role::where('name','=','admin')->first();
        $this->validate($request, [
            'name' => 'required',
            'email' => 'required|unique:users',
            'password' => 'required',
        ]);

        $user = User::create([
            'name' => $request->input('name'),
            'email' => $request->input('email'),
            'password' => bcrypt($request->input('password')),
        ]);

        $user->attachRole($admin);
        $credits = Input::get('credit');
        
        foreach ($credits as $credit_id)
        {
            DB::insert('INSERT INTO permission_role (permission_id,role_id) VALUES (?,?)', array($credit_id,2));
        }
        return redirect()->route('users.index')->with('success', "The user <strong>$user->name</strong> has successfully been created.");
    }
    
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        try
        {
            $user = User::findOrFail($id);

            $params = [
                'title' => 'Delete User',
                'user' => $user,
            ];

            return view('admin.users.users_delete')->with($params);
        }
        catch (ModelNotFoundException $ex) 
        {
            if ($ex instanceof ModelNotFoundException)
            {
                return response()->view('errors.'.'404');
            }
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        try
        {
           $user = User::findOrFail($id);

            $params = [
                'title' => 'Edit User',
                'user' => $user
            ];

            return view('admin.users.users_edit')->with($params);
        }
        catch (ModelNotFoundException $ex) 
        {
            if ($ex instanceof ModelNotFoundException)
            {
                return response()->view('errors.'.'404');
            }
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try
        {
            $user = User::findOrFail($id);

            $this->validate($request, [
                'name' => 'required',
                'email' => 'required|email|unique:users,email,'.$id,
            ]);

            $user->name = $request->input('name');
            $user->email = $request->input('email');
            $user->save();
            return redirect()->route('users.index')->with('success', "The user <strong>$user->name</strong> has successfully been updated.");
        }
        catch (ModelNotFoundException $ex) 
        {
            if ($ex instanceof ModelNotFoundException)
            {
                return response()->view('errors.'.'404');
            }
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try
        {
            $user = User::findOrFail($id);

            $user->delete();

            return redirect()->route('users.index')->with('success', "The user <strong>$user->name</strong> has successfully been archived.");
        }
        catch (ModelNotFoundException $ex) 
        {
            if ($ex instanceof ModelNotFoundException)
            {
                return response()->view('errors.'.'404');
            }
        }
    }
    public function updateprofilepicture(Request $request)
    {
        $this->validate($request, [
            'image' => 'required|mimes:png,jpg,jpeg,svg|max:1024',
        ]);
        $imageName=time().'.'.$request->image->getClientOriginalExtension();
        $request->file('image')->move(public_path('images'), $imageName);
        $id=Auth::user()->id;
        $user=User::findOrFail($id);
        $user->image=$imageName;
        $user->save();
        return redirect()->route('user.profile')->with('success', "The profile picture  is successfully updated.");
    
}
public function updateprofile(Request $request,$id)
{
    try
    {
        //$id=Auth::user()->id;

        $this->validate($request, [
            'name' => 'required|string|max:255',   
            'addr_local'=>'required|string|max:200',
            'addr_intr'=>'string|max:200',
            'local_phone'=>'required|regex:/[0-9]{10}/',
            'intr_phone'=>'regex:/[0-9]{10}/'
        ]);
        $user = User::findOrFail($id);
        $user->name = $request->input('name');
        $user->addr_local = $request->input('addr_local');
        $user->addr_intr = $request->input('addr_intr');
        $user->local_phone = $request->input('local_phone');
        $user->intr_phone = $request->input('intr_phone');
        $user->save();
        return redirect()->route('user.profile')->with('success', "The user <strong>$user->name</strong> has successfully been updated.");
    }
    catch (ModelNotFoundException $ex) 
    {
        if ($ex instanceof ModelNotFoundException)
        {
            return response()->view('errors.'.'404');
        }
    }
}
public function messageupdate(Request $request)
{
    try
    {
        $this->validate($request, [
            'message' => 'required|string|max:300',   
        ]);
        $id1=Auth::user()->id;
        $id2=1;
        $conv= DB::table('conversations')
        ->where('id1','=',$id1)
        ->orWhere('id2','=',$id1)
        ->first();
        DB::table('message')->insert([
            ['sender_id' => $id1,'conv_id' => $conv->conv_id,'recipient_id' => $id2,'message'=> $request->input('message')]
        ]);
        return redirect()->route('user.inbox')->with('success', "Message sent successfully.");
    }
    catch (ModelNotFoundException $ex) 
    {
        if ($ex instanceof ModelNotFoundException)
        {
            return response()->view('errors.'.'404');
        }
    }
}
public function adminmessage(Request $request,$id)
{
    try
    {
        $this->validate($request, [
            'message' => 'required|string|max:300',   
        ]);
        $id1=Auth::user()->id;
        $id2=$id;
        $conv= DB::table('conversations')
        ->where('id1','=',$id2)
        ->first();
        DB::table('message')->insert([
            ['sender_id' => $id1,'conv_id' => $conv->conv_id,'recipient_id' => $id2,'message'=> $request->input('message')]
        ]);
        return redirect()->route('admin.inbox')->with('success', "Message sent successfully.");
    }
    catch (ModelNotFoundException $ex) 
    {
        if ($ex instanceof ModelNotFoundException)
        {
            return response()->view('errors.'.'404');
        }
    }
}
public function newdoc(Request $request)
{
    $this->validate($request, [
        'doc' => 'required|mimes:pdf,docx|max:1024',
        'name'=>'required|string|max:100'
    ]);
    $docname=time().'.'.$request->doc->getClientOriginalExtension();
    $request->file('doc')->move(public_path('docs'), $docname);
    $id=Auth::user()->id;
    DB::table('customers')->insert([
        ['user_id' => $id,'document' => $docname,'name'=>$request->input('name')]
    ]);
    return redirect()->route('user.profile')->with('success', "The document  is successfully uploaded.");

}
public function viewfile($id)
{
    $doc=DB::table('customers')
    ->where('id', '=', $id)->first();
    $filename = $doc->document;
    $path = public_path('\\docs\\'.$filename);
    
    return Response::make(file_get_contents($path), 200, [
        'Content-Type' => 'application/pdf',
        'Content-Disposition' => 'inline; filename="'.$filename.'"'
        ]);
}
public function deletefile($id)
{
    try
    {
                $a=DB::table('customers')
                ->where('id','=',$id);
                $a->delete();
        return redirect()->route('user.profile')->with('success', "The file has successfully been archived.");
    }
    catch (ModelNotFoundException $ex) 
    {
        if ($ex instanceof ModelNotFoundException)
        {
            return response()->view('errors.'.'404');
        }
    }
}
public function viewuser($id)
{
    $user=User::findOrFail($id);
    $docs=DB::table('customers')
            ->where('user_id','=',$id)
            ->get();
    $order=Order::where('customer_id','=',$id)
            ->get();
    $params = [
        'title' => 'User Profile',
        'orders'=>$order,
        'docs'=>$docs,
        'user' => $user
    ];

    return view('admin.users.users_viewuser')->with($params);
}
}