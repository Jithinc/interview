<?php

namespace Larashop\Http\Controllers\Auth;

use Larashop\Models\User;
use Larashop\Models\Role;
use Larashop\Models\Conversation;

use Validator;
use Larashop\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Auth;
use Illuminate\Support\Facades\DB;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after login / registration.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }
    public function showRegisterForm()
    {
        $roles = Role::all();
        
                $params = [
                    'title' => 'Create User',
                    'roles' => $roles,
                ];
        
                return view('auth.register')->with($params);
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:6|confirmed',
            'image'=>'required',
            'image.*' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048'
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return User
     */
    /*protected function create(array $data)
    {
        return User::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'password' => bcrypt($data['password']),
        ]);
    }*/
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|min:6',
            'confirm_password' => 'required|min:6|same:password',          
            'image' => 'required|mimes:png,jpg,jpeg,svg|max:1024',
            'addr_local'=>'required|string|max:200',
            'addr_intr'=>'string|max:200',
            'local_phone'=>'required|regex:/[0-9]{10}/',
            'intr_phone'=>'regex:/[0-9]{10}/'
            
        ]);
        $imageName=time().'.'.$request->image->getClientOriginalExtension();
        $request->file('image')->move(public_path('images'), $imageName);
                    
                    
            /*$image = $request->file('image');
            $path = public_path().'/images/';
            $filename =$image->getClientOriginalName();
            $image->move($path, $filename);
            $extension = $image->getClientOriginalExtension();
            $filename = time().''.str_random(6).'.'.$extension;
            $image->move($destinationPath, $filename);*/
        $user = User::create([
            'name' => $request->input('name'),
            'email' => $request->input('email'),
            'password' => bcrypt($request->input('password')),
            'image'=>$imageName,
            'addr_local'=>$request->input('addr_local'),
            'local_phone'=>$request->input('local_phone'),
        ]);
        if($request->input('addr_intr')!=null){
            $user->addr_intr=$request->input('addr_intr');
        }
        if($request->input('intr_phone')!=null){
            $user->intr_phone=$request->input('intr_phone');
        }
        $user->save();
        //$role = Role::find($request->input('role_id'));
        $user->attachRole(2);
        $i=1;
        Conversation::create([
            'id1' => $user->id,
            'id2' => $i,
        ]);
        $conv=Conversation::where('id1','=',$user->id)
                            ->orWhere('id2','=',$user->id)
                            ->first();
        DB::table('message')->insert([
            ['conv_id' => $conv->conv_id,'sender_id' => $i,'recipient_id' => $user->id,'message'=> 'Welcome to hallel services !']
        ]);
        return redirect()->route('login')->with('success', "The user  has successfully been created.");
    }

    public function updateprofilepicture(Request $request,$id)
    {
        $this->validate($request, [
            'image' => 'required|mimes:png,jpg,jpeg,svg|max:1024',
        ]);
        $imageName=time().'.'.$request->image->getClientOriginalExtension();
        $request->file('image')->move(public_path('images'), $imageName);
        $id=Auth::user()->id;
        $user = User::findOrFail($id);
        $user->image=$imageName;
        $user->save();
        
        
        
        
        return redirect()->route('user.profile')->with('success', "The profile picture  is successfully updated.");
    }
    public function updateprofile(Request $request)
    {
        try
        {
            $id=Auth::user()->id;
            $user = User::findOrFail($id);

            $this->validate($request, [
                'name' => 'required|string|max:255',
                'email' => 'required|string|email|max:255|unique:users',      
                'addr_local'=>'required|string|max:200',
                'addr_intr'=>'string|max:200',
                'local_phone'=>'required|regex:/[0-9]{10}/',
                'intr_phone'=>'regex:/[0-9]{10}/'
            ]);

            $user->name = $request->input('name');
            $user->email = $request->input('email');
            $user->addr_local = $request->input('addr_local');
            $user->addr_intr = $request->input('addr_intr');
            $user->local_phone = $request->input('local_phone');
            $user->intr_phone = $request->input('intr_phone');
            $user->save();
            return redirect()->route('user.profile')->with('success', "The user <strong>$user->name</strong> has successfully been updated.");
        }
        catch (ModelNotFoundException $ex) 
        {
            if ($ex instanceof ModelNotFoundException)
            {
                return response()->view('errors.'.'404');
            }
        }
    }

}
