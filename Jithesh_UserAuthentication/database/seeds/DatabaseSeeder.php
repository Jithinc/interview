<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'name' => 'admin',
            'email' => 'admin@gmail.com',
            'password' => bcrypt('secret'),
        ]);
        DB::table('roles')->insert([
            'name' => 'master-admin',
            'display_name' => 'master',
            'description' => 'Master admin to manage all admin users',
        ]);
        DB::table('roles')->insert([
            'name' => 'admin',
            'display_name' => 'admin',
            'description' => 'Admin',
        ]);
        DB::table('role_user')->insert([
            'role_id' => '1',
            'user_id' => '1',
        ]);
        DB::table('permissions')->insert([
            'name' => 'manage-users',
            'display_name' => 'manage-users',
            'description'=>'Permission to manage all users'
        ]);
        DB::table('permissions')->insert([
            'name' => 'list-category',
            'display_name' => 'List Category',
            'description'=>'Permission to list all categories'
        ]);
        DB::table('permissions')->insert([
            'name' => 'add-products',
            'display_name' => 'Add Products',
            'description'=>'Permission to add new products'
        ]);
        DB::table('permission_role')->insert([
            'permission_id' => '1',
            'role_id' => '1',
        ]);
        DB::table('categories')->insert([
            'name' => 'Electronics',
            'description' => 'Electronic Equipments',
        ]);
        DB::table('categories')->insert([
            'name' => 'Fashion',
            'description' => 'Fashion Equipments',
        ]);
    }
}
