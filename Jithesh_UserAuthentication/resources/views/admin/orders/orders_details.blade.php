    @extends('templates.admin.layout')

    @section('content')
    <div class="">

        <div class="row">

            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <div class="x_title">
                        <h2><u>{{$order->service_name}}</u> &nbsp;&nbsp;<a href="{{url('admin/orders')}}" class="btn btn-info btn-xs"><i class="fa fa-chevron-left"></i> Back </a></h2>
                        <a href="{{url('admin/orders/delete/'.$order->id)}}" class="btn btn-danger btn-sm" style="float: right;"><i class="fa fa-trash"></i></a>
                        <div class="clearfix"></div>
                        <div class="clearfix"></div>
                        <div class="row">
                        <div class="col-md-6">
                        <form method="post" action="{{ url('admin/orders/addfile/'.$order->id) }}" enctype="multipart/form-data">
                        <div class="form-group{{ $errors->has('file') ? ' has-error' : '' }}">
                                <label class="control-label col-md-3 col-sm-3 col-xs-3" for="image">Image <span class="required">*</span>
                                </label><label>(jpg,png,svg acceptable..maximum size is 1mb)</label>
                                <input type="file" name="file" class="form-control"/>     
                                @if ($errors->has('file'))
                                    <span class="help-block">{{ $errors->first('file') }}</span>
                                    @endif
                            </div>
                            <div class="form-group">
                                <div >
                                    <input type="hidden" name="_token" value="{{ Session::token() }}">
                                    <button type="submit" class="btn btn-success">Submit</button>
                                </div>
                            </div>
                        </form>
                        </div>
                        <div class="col-md-6">
                        <form method="post" action="{{ url('admin/orders/addvideofile/'.$order->id) }}" enctype="multipart/form-data">
                        <div class="form-group{{ $errors->has('video') ? ' has-error' : '' }}">
                                <label class="control-label col-md-3 col-sm-3 col-xs-3" for="video">Video <span class="required">*</span>
                                </label><label>(mp4,avi,flv acceptable..maximum size is 10mb)</label>
                                <input type="file" name="video" class="form-control"/>     
                                @if ($errors->has('video'))
                                    <span class="help-block">{{ $errors->first('video') }}</span>
                                    @endif
                            </div>
                            <div class="form-group">
                                <div >
                                    <input type="hidden" name="_token" value="{{ Session::token() }}">
                                    <button type="submit" class="btn btn-success">Submit</button>
                                </div>
                            </div>
                        </form>
                        </div>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content">
                    <center>
                        <table id="datatable-buttons" class="table table-striped">
                            <tr><td><label>Service Name<label></td>
                            <td>{{$order->service_name}}</td></tr>
                            <tr><td><label>Service Price<label></td>
                            <td><i class="fa fa-inr" aria-hidden="true"></i>&nbsp;{{$order->total_amount}}</td></tr>
                            <tr><td><label>Service Added Date<label></td>
                            <td>{{$order->created_at}}</td></tr>
                        </table>
                        </center>
                        <div class="clearfix"></div>
        <div class="row">
        <center><h3><u>Uploaded Images</u></h3> </center>  
        @foreach($image as $image)
        <div class="col-md-3">
            <img src="/images/{{ $image->file }}" width="150" height="100" />
        </div>
        @endforeach
        </div>
        <div class="clearfix"></div>
        <div class="row">
        <center><h3><u>Uploaded Videos</u></h3></center>
        @foreach($video as $row)
        <div class="col-md-3">
            <video style="width: 100%" controls><source src="/videos/{{$row->file}}" type="video/mp4">
            </video>
        </div>
        @endforeach
        </div>
                    
                    </div>
                </div>
            </div>
        </div>
    </div>
    <style>
    a.btn btn-danger btn-xs {
        float: right;
    }
    h3 {
        font-family:Times New Roman;
    }</style>
    @stop