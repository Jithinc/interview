@extends('templates.admin.layout')

@section('content')
<div class="">

    <div class="row">

        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">
                <div class="x_title">
                    <h2>{{$order->service_name}} &nbsp;&nbsp;<a href="{{url('admin/user-orders')}}" class="btn btn-info btn-xs"><i class="fa fa-chevron-left"></i> Back </a></h2>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">
                <center>
                    <table id="datatable-buttons" class="table table-striped">
                        <tr><td><label>Service Name<label></td>
                        <td>{{$order->service_name}}</td></tr>
                        <tr><td><label>Service Price<label></td>
                        <td><i class="fa fa-inr" aria-hidden="true"></i>&nbsp;{{$order->total_amount}}</td></tr>
                        <tr><td><label>Service Added Date<label></td>
                        <td>{{$order->created_at}}</td></tr>
                    </table>
                    </center>
                    <h3><u>Uploaded Images</u></h3>     
        <div class="row">
        @foreach($image as $image)
        <div class="col-md-3">
            <img src="/images/{{ $image->file }}" width="150" height="100" />
        </div>
        @endforeach
        </div>
                </div>
            </div>
        </div> 
    </div>
</div>
@stop