@extends('templates.admin.layout')

    @section('content')
    <div class="">

        <div class="row">

            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <div class="x_title">
                        <h2>User Orders</h2>
                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content">
                        <table id="datatable-buttons" class="table table-striped table-bordered">
                            <thead>
                                <tr>                                
                                    <th>Service ID</th>
                                    <th>Service Name</th>
                                    <th>Amount</th>
                                    <th>User</th>
                                    <th>Date</th>
                                </tr>
                            </thead>
                            <tfoot>
                                <tr>
                                <th>Service ID</th>
                                <th>Service Name</th>
                                <th>Amount</th>
                                <th>User</th>
                                <th>Date</th>
                                </tr>
                            </tfoot>
                            <tbody>
                                @if(count($order))
                                @foreach($order as $row)
                                <tr>
                                    <td>{{$row->service_id}}</td>
                                    <td><a href="{{url('admin/user-orders/'.$row->id ) }}">{{$row->service_name}}</a></td>
                                    <td>{{$row->total_amount}}</td>
                                    <td>{{$row->name}}</td>
                                    <td>{{$row->created_at}}</td>
                                </tr>
                                @endforeach
                                @endif
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @stop