@extends('templates.admin.layout')

@section('content')
<div class="">

    <div class="row">

        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">
                <div class="x_title">
                    <h2>Product Search</h2>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">
                <form method="post" action="{{ url('admin/products/search/result') }}" data-parsley-validate class="form-horizontal form-label-left">

                        <div class="form-group{{ $errors->has('category') ? ' has-error' : '' }}">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="category">Category <span class="required">*</span>
                            </label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                            <select class="form-control m-bot15" name="category">
                                <option value="villa">Villa</option>    
                                <option value="3BHK">3BHK</option>    
                                <option value="2BHK">2BHK</option>    
                            </select>
                                @if ($errors->has('category'))
                                <span class="help-block">{{ $errors->first('category') }}</span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('type') ? ' has-error' : '' }}">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="type">Type <span class="required">*</span>
                            </label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                            <select class="form-control m-bot15" name="type">
                            <option value="sell">sell</option>    
                            <option value="buy">buy</option>    
                        </select>                                
                        @if ($errors->has('type'))
                                <span class="help-block">{{ $errors->first('type') }}</span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('location') ? ' has-error' : '' }}">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="location">Location<span class="required">*</span>
                            </label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                            <select class="form-control m-bot15" name="location">
                            @foreach($loc as $row)
                            <option value="{{$row->location}}">{{$row->location}}</option>                             
                               @if ($errors->has('location'))
                                <span class="help-block">{{ $errors->first('location') }}</span>
                                @endif
                                @endforeach
                                </select>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                                <input type="hidden" name="_token" value="{{ Session::token() }}">
                                <button type="submit" class="btn btn-success">search</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@stop