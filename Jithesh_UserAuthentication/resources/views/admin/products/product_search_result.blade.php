@extends('templates.admin.layout')

@section('content')
<div class="">

    <div class="row">

        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">
                <div class="x_title">
                    <h2>Search Result &nbsp;&nbsp;<a href="{{url('admin/products/search')}}" class="btn btn-info btn-xs"><i class="fa fa-chevron-left"></i> Back </a></h2>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">
                <table id="datatable-buttons" class="table table-striped table-bordered">
                        <thead>
                            <tr>
                            <th>Category</th>
                            <th>Location</th>
                            <th>Type</th>
                            </tr>
                        </thead>
                        <tfoot>
                            <tr>
                                <th>Category</th>
                                <th>Location</th>
                                <th>Type</th>
                            </tr>
                        </tfoot>
                        <tbody>
                            @if(count($product))
                            @foreach ($product as $row)
                            <tr>
                                <td>{{$row->category}}</td>
                                <td>{{$row->location}}</td>
                                <td>{{$row->type}}</td>
                            </tr>
                            @endforeach
                            @endif
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@stop