@extends('templates.admin.layout')

@section('content')
<div class="">

    <div class="row">

        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">
                <div class="x_title">
                    <h2>Services @permission('edit-service') <a href="{{route('services.create')}}" class="btn btn-primary btn-xs"><i class="fa fa-plus"></i> Create New </a>@endpermission</h2>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">
                  <table id="datatable-buttons" class="table table-striped table-bordered">
                        <thead>
                            <tr>
                                <th>Service</th>
                                @ability('','edit-service,delete')
                                <th>Action</th>
                                @endability
                            </tr>
                        </thead>
                        <tfoot>
                            <tr>
                                <th>Service</th>
                                @ability('','edit-service,delete')
                                <th>Action</th>
                                @endability
                            </tr>
                        </tfoot>
                        <tbody>
                            @if (count($services))
                            @foreach($services as $row)
                            <tr>
                                <td><a href="{{url('admin/services/view',$row->id)}}">{{$row->name}}</a></td>
                                @ability('','edit-service,delete')
                                <td>
                                    @permission('edit-service')
                                    <a href="{{ route('services.edit', ['id' => $row->id]) }}" class="btn btn-info btn-xs"><i class="fa fa-pencil" title="Edit"></i> </a>
                                    @endpermission
                                    @permission('delete')
                                    <a href="{{ route('services.show', ['id' => $row->id]) }}" class="btn btn-danger btn-xs"><i class="fa fa-trash-o" title="Delete"></i> </a>
                                    @endpermission
                                </td>
                                @endability
                            </tr>
                            @endforeach
                            @endif
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>


@stop