@extends('templates.admin.layout')

@section('content')
<div class="">

    <div class="row">

        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">
                <div class="x_title">
                    <h2>{{$service->name}} <a href="{{route('services.index')}}" class="btn btn-primary btn-xs"><i class="fa fa-chevron-left"></i> back </a>
                    @permission('edit-service')
                    <a href="{{url('admin/services/view/'.$service->id.'/create')}}" class="btn btn-primary btn-xs"><i class="fa fa-plus"></i> Create New </a></h2>
                    @endpermission
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">
                    <table id="datatable-buttons" class="table table-striped table-bordered">
                        <thead>
                            <tr>
                                <th>Service Name</th>
                                <th>Price</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tfoot>
                            <tr>
                                <th>Service Name</th>
                                <th>Price</th>
                                <th>Action</th>
                            </tr>
                        </tfoot>
                        <tbody>
                            @if (count($services))
                            @foreach($services as $row)
                            <tr>
                                <td>{{$row->service_name}}</td>
                                <td><i class="fa fa-inr" aria-hidden="true"></i> {{$row->price}}</td>
                                <td>                               
                                     <button type="button" class="btn btn-info btn-sm" data-toggle="modal" data-target="#{{$row->ser_id}}">View details</button>
                                     @permission('service-book')
                                    <a href="{{url('admin/services/addorders/'.$row->ser_id)}}" class="btn btn-primary btn-sm">Add To Orders</a>
                                     @endpermission
                                @ability('','edit-service,delete')
                                    @permission('edit-service')
                                    <a href="{{url('admin/services/view/'.$row->id.'/edit/'.$row->ser_id)}}"><i class="fa fa-pencil" title="Edit"></i></a>
                                    <a  href="{{url('admin/services/view/'.$row->id.'/delete/'.$row->ser_id)}}"
                                    onclick="return confirm('Are you sure you want to delete this item?');">
                                    <i class="fa fa-trash" title="Delete"></i></a>
                                    @endpermission
                                @endability
                                </td>
                            </tr>
<div id="{{$row->ser_id}}" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title"><b>{{$row->service_name}}</b></h4>
      </div>
      <div class="modal-body">
      <Label>Service Name:</label>{{$row->service_name}}<br>
      <Label>Price:</label><i class="fa fa-inr" aria-hidden="true"></i> {{$row->price}}<br>
      <Label>Description:</label>{{$row->description}}<br>

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>
                            @endforeach
                            @endif
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@stop