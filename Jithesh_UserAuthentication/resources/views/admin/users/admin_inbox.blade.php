@extends('templates.admin.layout')

    @section('content')
    <div class="">

        <div class="row">

            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <div class="x_title">
                        <h2>Inbox </h2>
                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content">
                        <table id="datatable-buttons" class="table table-striped table-bordered">
                            <thead>
                                <tr>
                                <td>User</td>
                                </tr>
                            </thead>
                            <tfoot>
                                <tr>
                                <td>
                                User
                                </td>
                                </tr>
                            </tfoot>
                            <tbody>
                                @foreach ($messages as $row)
                                <tr>
                                    <td><a href="{{url('admin/messages/'.$row->conv_id)}}">
                                    <img src="/images/{{$row->image}}" class="left" alt="Avatar" style="width:100%;">{{$row->name}}</a>
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <style>
        img.left {
            float: left;
            max-width: 60px;
           max-height: 50px;
            width: 100%;
            margin-right: 20px;
            border-radius: 50%;
         } 
        </style>
    </div>
    @stop