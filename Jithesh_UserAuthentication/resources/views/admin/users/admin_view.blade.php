@extends('templates.admin.layout')

@section('content')
<div class="">

    <div class="row">

        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">
                <div class="x_title">
                    <h2>{{$user->name }}</h2>&nbsp;&nbsp;<a href="{{route('admin.inbox')}}" class="btn btn-info btn-xs"><i class="fa fa-chevron-left"></i> Back </a>
                    <div class="clearfix"></div>
                </div>
                <button class="btn btn-primary btn-sm" data-toggle="modal" data-target="#myModal">Reply</button>
                <div id="myModal" class="modal fade" role="dialog">
                    <div class="modal-dialog">

                        <!-- Modal content-->
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                <h4 class="modal-title">Reply</h4>
                            </div>
                            <div class="modal-body">
                                <form method="post" action="{{url('admin/profile/adminreply/'.$user->id) }}" >
                                    <div class="form-group{{ $errors->has('message') ? ' has-error' : '' }}">
                                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="message">Message <span class="required">*</span>
                                        </label>
                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                            <textarea id="message" name = "message"  rows="3" cols="10" class="form-control">
                                            </textarea> 
                                            @if ($errors->has('message'))
                                            <span class="help-block">{{ $errors->first('message') }}</span>
                                            @endif
                                        </div>
                                    </div>
                                    <button class="btn btn-sm btn-info">Send</button>
                                    <input type="hidden" name="_token" value="{{ csrf_token() }}"/>
                                </form> 
                            </div>
                        </div>
                    </div>
                </div>
                <div class="x_content">
                        @foreach($messages as $message)
                            @if(Auth::user()->id==$message->sender_id)
                            <div class="jumbotron">
                            <img src="/images/{{Auth::user()->image}}" class="left " alt="Avatar" style="width:100%;">
                                <p>{{$message->Message}}</p>
                                <span class="time-left">{{$message->created_at}}</span>
                            </div>
                            @else
                           <div class="jumbotron" style="background-color:white">
                           <img src="/images/{{$user->image}}" class="right" alt="Avatar" style="width:100%;">
                                <p align="right">{{$message->Message}}</p>
                                <span class="time-right">{{$message->created_at}}</span>
                            </div>
                        @endif 
                        @endforeach
                </div>
            </div>
        </div>
    </div>
    <style>
        .jumbotron{
            border: 1px solid #dedede;
            background-color: #f1f1f1;
            border-radius: 2px;
            padding: 5px;
            margin: 10px 0;
            font-family: "Times New Roman";
        }
        .jumbotron img.left {
           float: left;
           max-width: 60px;
           width: 100%;
           margin-right: 20px;
           border-radius: 50%;
        } 
        .jumbotron img.right {
            float: right;
            max-width: 60px;
            max-height: 50px;
            width: 100%;
            margin-left: 20px;
            border-radius:50%;
        }

        

        .jumbotron::after {
            content: "";
            clear: both;
            display: table;
        }

        .time-right {
            font-size:30%;
            float: right;
            color: #aaa;
        }

        .time-left {
            font-size:30%;
            float: left;
            color: #999;
        }
    </style>
</div>
@stop