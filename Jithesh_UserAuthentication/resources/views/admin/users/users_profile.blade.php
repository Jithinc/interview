@extends('templates.admin.layout')

@section('content')
<div class="">

    <div class="row">

        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">
                <div class="x_title">
                    <h2>{{Auth::user()->name}}</h2>
                    <div class="clearfix"></div>
                </div>
                
                {!! HTML::image('images/'.Auth::user()->image, '...', array('class' => 'img-responsive float:right',
                    'align'=>'right','width' => '150', 'height' => '100')) !!}
                    @permission('user-inbox')
                    <button class="btn btn-danger" data-toggle="modal" data-target="#myModal2"><i class="fa fa-pencil"></i> Upload Documents</button>
                    &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
                    &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
                    &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
                    &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
                    @endpermission
                    @permission('edit-brands')
                    &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
                    &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
                    &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
                    &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
                    &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; 
                    @endpermission
                    <button class="btn btn-primary" data-toggle="modal" data-target="#myModal1"><i class="fa fa-pencil"></i> Edit</button>
                    <button class="btn btn-success" data-toggle="modal" data-target="#myModal"><i class="fa fa-pencil"></i> Update Picture</button>
                    <table class="table table-responsive">
                    <tr><td>Name</td><td>{{Auth::user()->name}}</td></tr>
                    <tr><td>Mail ID</td><td>{{Auth::user()->email}}</td></tr>
                    <tr><td>Local Address</td><td>{{Auth::user()->addr_local}}</td></tr>
                    <tr><td>Local Mobile</td><td>{{Auth::user()->local_phone}}</td></tr>
@if(Auth::user()->addr_intr!=null)
                    <tr><td>International Address</td><td>{{Auth::user()->addr_intr}}</td></tr>
@endif
@if(Auth::user()->intr_phone!=null)
                    <tr><td>International Mobile</td><td>{{Auth::user()->intr_phone}}</td></tr>
@endif
                    </table>
<table class="table table-striped">

@if(count($docs))
<h2>Uploaded Documents</h2>
@foreach($docs as $row)
<tr>
<td>{{$row->name}} </td><td><a href="{{url('admin/profile/viewfile/'.$row->id)}}">{{$row->document}}</a></td>
<td><a href="{{url('admin/profile/deletefile/'.$row->id)}}" ><i class="fa fa-trash"></i></a>
</td>
</tr>
@endforeach
@endif
</table>
<div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Upload New Profile Picture</h4>
      </div>
      <div class="modal-body">
      <form method="post" action="{{url('admin/profile/update_pro_pic') }}" enctype="multipart/form-data">
      <div class="form-group{{ $errors->has('image') ? ' has-error' : '' }}">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="image">Image <span class="required">*</span>
                            </label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                            <input type="file" name="image" class="form-control"/>     
                               @if ($errors->has('image'))
                                <span class="help-block">{{ $errors->first('image') }}</span>
                                @endif
                            </div>
                        </div>
      <button class="btn btn-sm btn-info">Submit</button>
      <input type="hidden" name="_token" value="{{ csrf_token() }}"/>
      </form> 
      </div>
    </div>
  </div>
</div>
<div id="myModal1" class="modal fade " tabindex="-1" role="dialog">
  <div class="modal-dialog modal-lg">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Update Profile</h4>
      </div>
      <div class="modal-body">
      <form method="post" action="{{url('admin/profile/update/'.Auth::user()->id) }}" >
      <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Name <span class="required">*</span>
      </label>
      <div class="col-md-3 col-sm-3 col-xs-12">
          <input type="text" value="{{Auth::user()->name}}" id="name" name="name" class="form-control col-md-7 col-xs-12">
          @if ($errors->has('name'))
          <span class="help-block">{{ $errors->first('name') }}</span>
          @endif
      </div>
  </div>

  <div class="form-group{{ $errors->has('addr_local') ? ' has-error' : '' }}">
      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="addr_local">Local Address <span class="required">*</span>
      </label>
      <div class="col-md-3 col-sm-3 col-xs-12">
      <textarea id="addr_local" name = "addr_local"  rows="3" cols="10" class="form-control">{{Auth::user()->addr_local}}
                            </textarea> 
          @if ($errors->has('addr_local'))
          <span class="help-block">{{ $errors->first('addr_local') }}</span>
          @endif
      </div>
  </div>
  <div class="form-group{{ $errors->has('local_phone') ? ' has-error' : '' }}">
      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="local_phone">Local MObile Number <span class="required">*</span>
      </label>
      <div class="col-md-3 col-sm-3 col-xs-12">
      <input type="text" name = "local_phone" value="{{Auth::user()->local_phone}}" class="form-control">
                            </input> 
          @if ($errors->has('local_phone'))
          <span class="help-block">{{ $errors->first('local_phone') }}</span>
          @endif
      </div>
  </div>
@if(Auth::user()->addr_intr!=null)
<div class="form-group{{ $errors->has('addr_intr') ? ' has-error' : '' }}">
      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="addr_intr">International Address 
      </label>
      <div class="col-md-3 col-sm-3 col-xs-12">
      <textarea id="addr_intr" name = "addr_intr" rows="3" cols="10" class="form-control">
      {{Auth::user()->addr_intr}}
                            </textarea> 
          @if ($errors->has('addr_intr'))
          <span class="help-block">{{ $errors->first('addr_intr') }}</span>
          @endif
      </div>
  </div>
@endif
@if(Auth::user()->intr_phone!=null)
<div class="form-group{{ $errors->has('intr_phone') ? ' has-error' : '' }}">
      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="intr_phone">International Phone 
      </label>
      <div class="col-md-3 col-sm-3 col-xs-12">
      <input type="text" name = "intr_phone" value="{{Auth::user()->intr_phone}}" rows="3" cols="10" class="form-control">
                            </input> 
          @if ($errors->has('intr_phone'))
          <span class="help-block">{{ $errors->first('intr_phone') }}</span>
          @endif
      </div>
  </div>
@endif
      <button class="btn btn-sm btn-info">Submit</button>
      <input type="hidden" name="_token" value="{{ csrf_token() }}"/>
      </form> 
      </div>
    </div>
  </div>
</div>
<div id="myModal2" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Upload New Document</h4>
      </div>
      <div class="modal-body">
      <form method="post" action="{{url('admin/profile/newdoc') }}" enctype="multipart/form-data">
      <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Document Name <span class="required">*</span>
                            </label>
                            <div class="col-md-8 col-sm-8 col-xs-12">
                            <input type="text" name="name" class="form-control"/>     
                               @if ($errors->has('name'))
                                <span class="help-block">{{ $errors->first('name') }}</span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group{{ $errors->has('doc') ? ' has-error' : '' }}">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="doc">Document <span class="required">*</span>
                            </label>
                            <div class="col-md-8 col-sm-8 col-xs-12">
                            <input type="file" name="doc" class="form-control"/>     
                               @if ($errors->has('doc'))
                                <span class="help-block">{{ $errors->first('doc') }}</span>
                                @endif
                            </div>
                        </div>
<label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Maximum size of file should be less than 1mb.
pdf or docx will be allowed</label><br>
      <button class="btn btn-sm btn-info" style="float: right;">Submit</button>
      <input type="hidden" name="_token" value="{{ csrf_token() }}"/>
      </form> 
      </div>
    </div>
  </div>
</div>
 
            </div>
        </div>
    </div>
</div>
<style>
input[type=text] {
    width: 100%;
    padding: 12px 20px;
    margin: 8px 0;
    box-sizing: border-box;
}

table {
    font-family: "Times New Roman";
}
</style>

@stop