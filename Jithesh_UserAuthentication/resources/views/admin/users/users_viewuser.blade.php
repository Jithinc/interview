@extends('templates.admin.layout')

@section('content')
<div class="">

    <div class="row">

        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">
                <div class="x_title">
                    <h2>{{$user->name}}</h2>&nbsp;&nbsp;&nbsp;<a href="{{url('admin/users')}}" class="btn btn-info btn-xs"><i class="fa fa-chevron-left"></i> Back </a>
                    <div class="clearfix"></div>
                </div>
                
                <div class="container">
                <div class="row">
                <div class="col-sm-3">
                <center>
                {!! HTML::image('images/'.$user->image, '...', array('class' => 'img-responsive img-circle float:right',
                'width' => '120', 'height' => '80')) !!}
                    <br/>
                <label >{{$user->name}}</label></center>
                </div>
                <div class="col-sm-8">
                <div class="panel with-nav-tabs">
                <div class="panel-heading">
                        <ul class="nav nav-tabs">
                            <li class="active"><a href="#tab1info" data-toggle="tab">Basic Details</a></li>
                            <li><a href="#tab2info" data-toggle="tab">Uploaded Files</a></li>
                            <li><a href="#tab3info" data-toggle="tab">Selected Services</a></li>
                        </ul>
                </div>
                <div class="panel-body">
                    <div class="tab-content">
                        <div class="tab-pane fade in active" id="tab1info">
                            <h3>Details</h3>
                            <table class="table table-hover">
                            <tr><td>Name</td><td>{{$user->name}}</td></tr>
                            <tr><td>Email</td><td>{{$user->email}}</td></tr>
                            <tr><td>Local Address</td><td>{{$user->addr_local}}</td></tr>
                            <tr><td>Local Mobile Number</td><td>{{$user->local_phone}}</td></tr>
                            @if($user->addr_intr!=null)
                            <tr><td>International Address</td><td>{{$user->addr_intr}}</td></tr>
                            @endif
                            @if($user->intr_phone!=null)
                            <tr><td>International Mobile Number </td><td>{{$user->intr_phone}}</td></tr>
                            @endif
                            </table>
                        </div>
                        <div class="tab-pane fade in" id="tab2info">
                            <table class="table table-hover">
                
                            @if(count($docs))
                            <h3>Uploaded Documents</h3>
                            @foreach($docs as $row)
                            <tr>
                            <td>{{$row->name}} </td><td><a href="{{url('admin/profile/viewfile/'.$row->id)}}">{{$row->document}}</a></td>
                            </tr>
                            @endforeach
                            @endif
                            </table>
                        </div>
                        <div class="tab-pane fade in" id="tab3info">
                
                            @if(count($orders))
                            <h3>Selected Services</h3>
                            <table class="table table-hover">
                            <thead>
                        <th>Service Name</th>
                        <th>Service Charge</th></thead>
                            @foreach($orders as $row)
                        <tbody>
                            <tr>
                            <td>{{$row->service_name}} </td>
                            <td>{{$row->total_amount}}</td>
                            </tr></tbody>
                            @endforeach
                            @endif
                            </table>
                        </div>
                    </div>
                </div>
            </div>
                
                
                
                </div>
                </div>
                </div>
                
                   
 
            </div>
        </div>
    </div>
</div>

@stop