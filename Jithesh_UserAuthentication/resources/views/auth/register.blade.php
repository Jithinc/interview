<!DOCTYPE html>
<html lang="en">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <!-- Meta, title, CSS, favicons, etc. -->
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Register</title>

        <!-- Bootstrap -->
        <link href="{{asset('admin/css/bootstrap.min.css')}}" rel="stylesheet">
        <!-- Font Awesome -->
        <link href="{{asset('admin/css/font-awesome.min.css')}}" rel="stylesheet">
        <!-- NProgress -->
        <link href="{{asset('admin/css/nprogress.css')}}" rel="stylesheet">
        <!-- Animate.css -->
        <link href="{{asset('admin/css/animate.min.css')}}" rel="stylesheet">

        <!-- Custom Theme Style -->
       <!-- <link href="{{asset('admin/css/custom.min.css')}}" rel="stylesheet">-->
        <script type="text/javascript" src="js/bootstrap.min.js"></script> 
    </head>

    <body>
    <div class="container" style="max-width:600px;margin:60px auto;">
    <h3>Registration</h3>
                       <form role="form" method="post" action="{{ route('register') }}" enctype="multipart/form-data">

                        <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Name <span class="required">*</span>
                            </label>
                                <input type="text" value="{{ Request::old('name') ?: '' }}" id="name" name="name" class="form-control col-md-7 col-xs-12">
                                @if ($errors->has('name'))
                                <span class="help-block">{{ $errors->first('name') }}</span>
                                @endif
                        </div>

                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="email">Email <span class="required">*</span>
                            </label>
                                <input type="text" value="{{ Request::old('email') ?: '' }}" id="email" name="email" class="form-control col-md-7 col-xs-12">
                                @if ($errors->has('email'))
                                <span class="help-block">{{ $errors->first('email') }}</span>
                                @endif
                        </div>
                        <div class="form-group{{ $errors->has('addr_local') ? ' has-error' : '' }}">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="email">Local Address <span class="required">*</span>
                            </label>
                                <!--<input type="text" value="{{ Request::old('addr_local') ?: '' }}" id="addr_local" name="addr_local" class="form-control col-md-7 col-xs-12">
                                --><textarea id="addr_local" name = "addr_local" rows="3" cols="10" class="form-control">
                                {{{ Request::old('addr_local') }}}
                                </textarea>
                                @if ($errors->has('addr_local'))
                                <span class="help-block">{{ $errors->first('addr_local') }}</span>
                                @endif
                        </div>
                        <div class="form-group{{ $errors->has('local_phone') ? ' has-error' : '' }}">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="email">Mobile Number (Local)<span class="required">*</span>
                            </label>
                                <input type="text" value="{{ Request::old('local_phone') ?: '' }}" id="local_phone" name="local_phone" class="form-control col-md-7 col-xs-12">
                                @if ($errors->has('local_phone'))
                                <span class="help-block">{{ $errors->first('local_phone') }}</span>
                                @endif
                        </div>
                        <div class="form-group{{ $errors->has('addr_intr') ? ' has-error' : '' }}">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="email">International Address
                            </label>
                            <textarea id="addr_intr" name = "addr_intr" rows="3" cols="10" class="form-control">
                            {{{ Request::old('addr_intr') }}}
                            </textarea>                                
                            @if ($errors->has('addr_intr'))
                                <span class="help-block">{{ $errors->first('addr_intr') }}</span>
                                @endif
                        </div>
                        <div class="form-group{{ $errors->has('intr_phone') ? ' has-error' : '' }}">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="email">Mobile Number (International)
                            </label>
                                <input type="text" value="{{ Request::old('intr_phone') ?: '' }}" id="intr_phone" name="intr_phone" class="form-control col-md-7 col-xs-12">
                                @if ($errors->has('intr_phone'))
                                <span class="help-block">{{ $errors->first('intr_phone') }}</span>
                                @endif
                        </div>
                        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">Password<span class="required">*</span>
                            </label>
                                <input type="password" value="{{ Request::old('password') ?: '' }}" id="password" name="password" class="form-control col-md-7 col-xs-12">
                                @if ($errors->has('password'))
                                <span class="help-block">{{ $errors->first('password') }}</span>
                                @endif
                        </div>

                        <div class="form-group{{ $errors->has('confirm_password') ? ' has-error' : '' }}">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="confirm_password">Confirm Password <span class="required">*</span>
                            </label>
                                <input type="password" value="{{ Request::old('confirm_password') ?: '' }}" id="confirm_password" name="confirm_password" class="form-control col-md-7 col-xs-12">
                                @if ($errors->has('confirm_password'))
                                <span class="help-block">{{ $errors->first('confirm_password') }}</span>
                                @endif
                        </div>
                        <div class="form-group{{ $errors->has('image') ? ' has-error' : '' }}">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="image">Image <span class="required">*</span>
                            </label>
                            <input type="file" name="image" class="form-control"/>     
                               @if ($errors->has('image'))
                                <span class="help-block">{{ $errors->first('image') }}</span>
                                @endif
                        </div>
                        

                        <div class="form-group">
                            <div class="col-md-3 col-sm-3 col-xs-12 col-md-offset-3">
                                <input type="hidden" name="_token" value="{{ Session::token() }}">
                                <button type="submit" class="btn btn-success">Submit</button>
                            </div>
                        </div>
                    </form>
                    </section>
        </div>
    </body>
</html>