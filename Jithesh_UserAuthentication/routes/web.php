<?php
use \Http\Controllers\Admin\UsersController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/
Route::get('/', [
    'uses' => 'HomeController@index',
    'as' => 'home',
]);
Route::group(['prefix' => 'admin','middleware' => 'auth','namespace' => 'Admin'],function(){
    Route::get('products/search','ProductsController@searchitem');   
    Route::post('products/search/result','ProductsController@searchresult');            
    Route::resource('customers', 'CustomersController');
    Route::resource('brands', 'BrandsController');
    Route::resource('product-categories', 'ProductCategoriesController');
    Route::resource('products', 'ProductsController');
    Route::post('profile/update_pro_pic','UsersController@updateprofilepicture');  
    Route::post('profile/update/{id}','UsersController@updateprofile');  
    Route::post('profile/newdoc','UsersController@newdoc');     
    Route::get('profile/viewfile/{id}','UsersController@viewfile');
    Route::get('profile/deletefile/{id}','UsersController@deletefile');          
    Route::get('profile', ['uses'=>'UsersController@view','as'=>'user.profile']); 
    Route::get('messages', ['uses'=>'UsersController@viewmessage','as'=>'admin.inbox']);   
    Route::get('messages/{conv_id}','UsersController@viewmes');
    Route::get('inbox', ['uses'=>'UsersController@viewinbox','as'=>'user.inbox']);
    Route::post('profile/messagereply','UsersController@messageupdate');  
    Route::post('profile/adminreply/{id}','UsersController@adminmessage'); 
    Route::get('users/view/{id}','UsersController@viewuser');    
    Route::resource('users', 'UsersController');
    Route::get('services/view/{id}', 'ServiceController@view');   
    Route::get('services/view/{id}/edit/{ser_id}', 'ServiceController@editservice'); 
    Route::post('services/view/{id}/edit/{ser_id}', 'ServiceController@updateservice'); 
    Route::get('services/view/{id}/create','ServiceController@newservice'); 
    Route::post('services/view/{id}/create','ServiceController@createservice'); 
    Route::get('services/view/{id}/delete/{ser_id}','ServiceController@destroyservice'); 
    Route::get('services/addorders/{id}','OrdersController@addorder'); 
    Route::get('orders/{id}','OrdersController@view');
    Route::get('user-orders',['uses'=>'OrdersController@vieworders','as'=>'order.orders']);     
    Route::get('user-orders/{id}','OrdersController@viewuserorder');
    Route::post('orders/addfile/{id}','OrdersController@addfile');
    Route::post('orders/addvideofile/{id}','OrdersController@addvideofile');    
    Route::get('orders/delete/{id}','OrdersController@destroy');     
    Route::resource('services', 'ServiceController');
    //Route::get('services/view/{id}/edit/{ser_id}', 'ServiceController@editservice');        
    Route::get('orders',[
        'uses' => 'OrdersController@index',
        'as' => 'orders.index',
    ]);
});

//Auth::routes();
Route::group(['prefix' => 'admin','namespace' => 'Auth'],function(){
    // Authentication Routes...  
    Route::get('login', 'LoginController@showLoginForm')->name('login');
    Route::post('login', 'LoginController@login');
    Route::get('register', 'RegisterController@showRegisterForm')->name('register');
    Route::post('register', 'RegisterController@store');
    Route::post('logout', 'LoginController@logout')->name('logout');

    // Password Reset Routes...
    Route::get('password/reset', 'ForgotPasswordController@showLinkRequestForm')->name('password.reset');
    Route::post('password/email', 'ForgotPasswordController@sendResetLinkEmail')->name('password.email');
    Route::get('password/reset/{token}', 'ResetPasswordController@showResetForm')->name('password.reset.token');
    Route::post('password/reset', 'ResetPasswordController@reset');
});

Route::get('/home', 'HomeController@index');
Route::get('register','UsersController@showregform');
Route::post('register', 'UsersController@store1');